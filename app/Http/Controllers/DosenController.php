<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function index(Request $request)
    {
       $data_guru=\App\Guru::all();
       return view('Dosen.tabeldosen',['data_guru'=>$data_guru]);   
    }

    public function create(Request $request)
    {
        $this->validate($request,[
            'nama'=>'required|min:5',
            'telpon'=>'required|min:5',
            'email'=>'required|email|unique:users',
            'alamat'=>'required|min:5',
        ]);

         //insert ke table user
         $user= new\App\User;
         $user->role='guru';
         $user->name=$request->nama;
         $user->email=$request->email;
         $user->password= bcrypt('qweasd123');
         $user->remember_token=str_random(60);
         $user->Save();

        // insert ke table guru
        $request->request->add(['user_id'=>$user->id]);
        $guru= \App\Guru::create($request->all()); 
    return redirect('/dosen')->with('sukses','Data Berhasil Ditambahkan');
    }
  public function delete($id)
  {
    $guru=\App\Guru::find($id);
       $guru->delete($id);


        return redirect('/dosen')->with('sukses','Data Berhasil Di hapus !');
  }
  public function edit($id)
  {
    $guru=\App\Guru::find($id);
    return view('Dosen/edit',['guru'=>$guru]);
  }
  public function update(Request $request,$id)
  {
    $guru=\App\guru::find($id);
      $guru->update($request->all());
      
      return redirect('/dosen')->with('sukses','Data Berhasil Di Ubbah !');
  }
}
