<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class SiswaController extends Controller
{
    public function index()
    {
    	$data_siswa=\App\Siswa::all();
    	return view('siswa.index',['data_siswa'=>$data_siswa]);
    }
    public function create(Request $request)
    {
       $this->validate($request,[
        'nama_depan'=> 'required|min:4',
        'nama_belakang'=>'required',
        'email'=>'required|email|unique:users',
        'jenis_kelamin'=>'required',
        'agama'=>'required',
    ]);
       


        //insert ke table user
        $user= new\App\User;
        $user->role='siswa';
        $user->name=$request->nama_depan;
        $user->email=$request->email;
        $user->password= bcrypt('qweasd123');
        $user->remember_token=str_random(60);
        $user->Save();

        //insert ke table siswa
        $request->request->add(['user_id'=>$user->id]);
        $siswa= \App\Siswa::create($request->all());  
           
    	return redirect('/siswa')->with('sukses','Data Di Tambahkan');
    }
    public function edit($id)
    {
     
    	$Siswa=\App\Siswa::find($id);
    	return view('siswa/edit',['Siswa'=>$Siswa]);
    }
    public function update(Request $request,$id)
    {
        //dd($request->all());
    	$siswa=\App\Siswa::find($id);
    	$siswa->update($request->all());

        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $siswa->avatar=$request->file('avatar')->getClientOriginalName();
            $siswa->Save();
        }
        


    	return redirect('/siswa')->with('sukses','Data Berhasil di Update');
    }
    public function delete($id)
    {
       $Siswa=\App\Siswa::find($id);
       $Siswa->delete($id);


        return redirect('/siswa')->with('sukses','Data Berhasil Dihapus !');
    }
    public function profile($id)
    {
     $siswa=\App\Siswa::find($id);
     $matapelajaran=\App\Mapel::all();
       return view('siswa.profile',['siswa'=>$siswa,'matapelajaran'=>$matapelajaran]); 
    }

    public function addnilai(Request $request,$idsiswa)
    {
        //dd untuk check apakah data masuk atau belum
        //dd($request->all());
         $siswa=\App\Siswa::find($idsiswa);
         if($siswa->mapel()->where('mapel_id',$request->mapel)->exists()){

            return redirect('siswa/'.$idsiswa.'/profile')->with('gagal','Data Sudah Ada');
         }

         $siswa->mapel()->attach($request->mapel,['nilai'=>$request->nilai]);

         return redirect('siswa/'.$idsiswa.'/profile')->with('berhasil','Nilai Berhasil di Tambah');

    }
    public function deletenilai($idsiswa,$idmapel)
    {
     $siswa=\App\Siswa::find($idsiswa);
     $siswa->mapel()->detach($idmapel);
     //Session::flash('sukses','Data Berhasil Di Hapus');  
     return redirect()->back();
    }

   public function profilsaya()
   {
       return view('siswa.profilsaya');
   }


}
