<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function home(){
    	return view('home');
    }
    public function about(){
    	return view('about');
    }
    public function dashboard(){
    	$data = ['nama' => 'bono','hobi' => 'balet','umur' => 47];
    	return view('dashboard', $data);
    }
    public function loginCheck(){
    	return redirect('/dashboard');
    }
}

