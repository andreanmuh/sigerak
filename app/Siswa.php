<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{

    protected $table	='siswa';
    protected $fillable =['nama_depan','nama_belakang','jenis_kelamin','agama','alamat','avatar','user_id'];


public function getAvatar()
{
	if(!$this->avatar){
		return asset('images/user.jpg');
	}
	return asset('images/'.$this->avatar);
}
public function mapel()
{
	return $this->belongsToMany(mapel::class)->withPivot(['nilai'])->withTimestamps();
}
	public function rataRataNilai()
	{
		$total=0;
		$hitung=0;
		foreach ($this->mapel as $mapel) {
			$total=$total + $mapel->pivot->nilai;
			$hitung++;
		}
		return $total==0 ? 0 : round($total/$hitung,1);

	}

	public function nama_lengkap()
	{
		return $this->nama_depan.' '.$this->nama_belakang;
	}


}