@extends('layout.master')

@section('konten')
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
<div class="col-md-7">
<div class="panel">

<!--Belum Berfungsi sama sekali, relasikan nama matkul dengan tabel mapel !!!!-->

					<div class="panel-heading">
                    <div class="right">
						<button type="button" class="btn" data-toggle="modal" data-target="#exampleModal">
						<i class="lnr lnr-plus-circle"></i></button>
					</div>
						<h3 class="panel-title">Jadwal Praktek Mahasiswa</h3>
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<thead>
								<tr><th>Mata Kuliah</th>
									<th>Hari</th>
									<th>Jam</th>
                                    <th>Aksi</th>
								</tr>
                                <tr>
                                <td>Otomotif</td>
                                <td>Senin</td>
                                <td>12.30 WITA</td>
                                <td><a href="#" class="btn btn-warning btn-sm">Edit</a>
	<a href="#" class="btn btn-danger btn-sm delete">Hapus</a></td>


                                </tr>

							</thead>
							<tbody>
					
							</tbody>
						</table>
					</div>
				</div>
			</div>
        


            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Jadwal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">



				        <form action="#" method="POST">
				        	{{csrf_field()}}

				  
				    <label for="matkul">Mata Kuliah</label>
				    <input name="nama_belakang" type="text" class="form-control" id="matkul" aria-describedby="emailHelp" placeholder="Nama Mata Kuliah">
			

				  
				    <label for="exampleInputEmail1">Hari</label>
				    <input name="hari" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Hari">
				    
			

				 
				  <label for="exampleInputEmail1">Jam</label>
				    <input name="jam" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="jam">
				
				

			<br>
				




				   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <button type="submit" class="btn btn-primary">Tambah</button>

</div>



@stop