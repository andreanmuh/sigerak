@extends('layout.master')

@section('header')

@stop

@section('konten')

<div class="main">

			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
	@if(session('berhasil'))
		<div class="alert alert-success" role="alert">
		  {{session('berhasil')}}
		</div>

		@endif
	@if(session('gagal'))
		<div class="alert alert-danger" role="alert">
		  {{session('gagal')}}
		</div>
		@endif		


<div class="panel panel-profile">
	<div class="clearfix">
		<!-- LEFT COLUMN -->
		<div class="profile-left">

			<!-- PROFILE HEADER -->
			<div class="profile-header">
				<div class="overlay"></div>
				<div class="profile-main">

					<img width="100" height="100" src="{{auth()->user()->siswa->getAvatar()}}" class="img-circle" alt="Avatar">
					<h3 class="name">{{auth()->user()->siswa->nama_depan}}</h3>
					<span class="online-status status-available">Available</span>
				</div>

				<div class="profile-stat">
					<div class="row">
						<div class="col-md-4 stat-item">
							{{auth()->user()->siswa->mapel->count()}} <span>Mata Kuliah</span>
						</div>
						<div class="col-md-4 stat-item">
							15 <span>Awards</span>
						</div>
						<div class="col-md-4 stat-item">
							2174 <span>Points</span>
						</div>
					</div>
				</div>
			</div>
			<!-- END PROFILE HEADER -->

			<!-- PROFILE DETAIL -->
			<div class="profile-detail">
				<div class="profile-info">
					<h4 class="heading">Info Siswa</h4>
					<ul class="list-unstyled list-justify">
						<li>Nama Lengkap <span>{{auth()->user()->siswa->nama_depan}} {{auth()->user()->siswa->nama_belakang}}</span></li>
						<li>Jenis Kelamin <span>{{auth()->user()->siswa->jenis_kelamin}}</span></li>
						<li>Agama <span>{{auth()->user()->siswa->agama}}</span></li>
						<li>Alamat <span>{{auth()->user()->siswa->alamat}}</span></li>
					</ul>
				</div>
				<div class="text-center"><a href="/siswa/{{auth()->user()->siswa->id}}/edit" class="btn btn-warning">Edit Profile</a></div>
			</div>
			<!-- END PROFILE DETAIL -->
		</div>
		<!-- END LEFT COLUMN -->

		<!-- RIGHT COLUMN -->
		<div class="profile-right">
			<!-- Button trigger modal -->
			@if(auth()->user()->role == 'admin')
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
		  Tambah Nilai
		</button>
		@endif
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Mata Pelajaran</h3>
				</div>
				<div class="panel-body">
					<table class="table table-striped">
						<thead>
							<tr><th>Kode</th>
								<th>Name</th>
								<th>Semester</th>
								<th>Nilai</th>
								<th>Guru</th>
								
								
							</tr>
						</thead>
						<tbody>
							
							@foreach(auth()->user()->siswa->mapel as $mapel)
							<tr><td>{{$mapel->kode}}</td>
								<td>{{$mapel->nama}}</td>
								<td>{{$mapel->semester}}</td>
								<td><a href="#" class="nilai" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{auth()->user()->siswa->id}}/editnilai" data-title=" Masukkan Nilai">{{$mapel->pivot->nilai}}</a></td>
								<td>{{$mapel->guru->nama}}</td>
					

					<td></td>

							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<!-- END TABBED CONTENT -->
		</div>
		<!-- END RIGHT COLUMN -->
	</div>
</div>

				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>




@stop

@section('footer')


<script>
	$(document).ready(function() {
    $('.nilai').editable();
});
</script>

@stop