@extends('layout.master')	<!-- mengambil template dari file master yg ada di layout -->
		<!-- meletakkan semua cript di bawah section sampai endsection, ('konten')penamaan dibuat di file master-->

@section('konten')

<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Siswa</h3>
				</div>
				 <form action="/siswa/{{$Siswa->id}}/update" method="POST" enctype="multipart/form-data">
						        	{{csrf_field()}}

						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama Depan</label>
						    <input name="nama_depan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama_depan" value="{{$Siswa->nama_depan}}">

						    <br>

						    <label for="exampleInputEmail1">Nama Belakang</label>
						    <input name="nama_belakang" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama_belakang" value="{{$Siswa->nama_belakang}}">

						    <br>

						    <div class="form-group">
						    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
						    <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
						      <option value="L" @if($Siswa->jenis_kelamin=='L')selected @endif>Laki-Laki</option>
						      <option value="P" @if($Siswa->jenis_kelamin=='P')selected @endif>Perempuan</option>
						    </select>
						    </div>
						 <br>

						  <label for="exampleInputEmail1">Agama</label>
						    <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Agama" value="{{$Siswa->agama}}">

						<br>

						    <label for="exampleFormControlTextarea1">Alamat</label>
						    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="4">{{$Siswa->alamat}}</textarea>   
						<br>

						    <label for="exampleFormControlTextarea1">Avatar</label>
						    <input type="file" name="avatar" class="form-control">

						  </div>
						  <button type="submit" class="btn btn-warning">Update</button>
						</form>  


					
				</div>
			</div>

				</div>
			</div>
		</div>
	</div>
</div>

@stop




@section('konten1') 	
		<h1>Edit Data Mahasiswa</h1>
		@if(session('sukses'))
		<div class="alert alert-success" role="alert">
		  {{session('sukses')}}
		</div>
		@endif

		<div v class="row">
			<div class="col-lg-12">
				------------------------------------------------------------------
					 <form action="/siswa/{{$Siswa->id}}/update" method="POST">
						        	{{csrf_field()}}

						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama Depan</label>
						    <input name="nama_depan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama_depan" value="{{$Siswa->nama_depan}}">

						    <br>

						    <label for="exampleInputEmail1">Nama Belakang</label>
						    <input name="nama_belakang" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama_belakang" value="{{$Siswa->nama_belakang}}">

						    <br>

						    <div class="form-group">
						    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
						    <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
						      <option value="L" @if($Siswa->jenis_kelamin=='L')selected @endif>Laki-Laki</option>
						      <option value="P" @if($Siswa->jenis_kelamin=='P')selected @endif>Perempuan</option>
						    </select>
						    </div>
						 <br>

						  <label for="exampleInputEmail1">Agama</label>
						    <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Agama" value="{{$Siswa->agama}}">

						<br>

						    <label for="exampleFormControlTextarea1">Alamat</label>
						    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="4">{{$Siswa->alamat}}</textarea>   
						  </div>
						  <button type="submit" class="btn btn-warning">Update</button>
						</form>  
						</div>
						</div>  
@endsection