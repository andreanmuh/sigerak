@extends('layout.master')

@section('konten')
@include('layout._navbar')
@include('layout._sidebar')
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
<div class="col-md-6">
<div class="panel">
					<div class="panel-heading">
						<h3 class="panel-title">TOP Lokal Kampus Mandiri</h3>
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<thead>
								<tr><th>Ranking</th>
									<th>Nama</th>
									<th>Nilai</th>
								</tr>
							</thead>
							<tbody>
								@php
									$ranking =1;
								@endphp

								@foreach(ranking5Besar() as $s)
								<tr>
									<td>{{$ranking}}</td>
									<td>{{$s->nama_lengkap()}}</td>
									<td>{{$s->rataRataNilai()}}</td>
								</tr>
								@php
									$ranking++;
								@endphp

								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
	<div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-user"></i></span>
								 <p>
									<span class="number">{{totalSiswa()}}</span>
									<span class="title">Jumlah Siswa</span>
									</p>
									</div>
								</div>
				<div class="col-md-3">
					<div class="metric">
						<span class="icon"><i class="lnr lnr-user"></i></span>
							<p>
							<span class="number">{{totalGuru()}}</span>
								<span class="title">Jumlah Guru</span>
							</p>
						</div>
					</div>
								

	</div>
</div>
</div>
</div>
@stop

@section('footer')
<script>
	swal("Selamat Datang :D");
</script>

@stop