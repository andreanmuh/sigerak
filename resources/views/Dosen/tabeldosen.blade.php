@extends('layout.master')

@section('konten')

<div class="main">
	<div class="main-content">
		<div class="container-fluid">

@if(session('sukses'))
		<div class="alert alert-success" role="alert">
		  {{session('sukses')}}
		</div>

		@endif

			<div class="row">
				<div class="col-md-12">
					<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Data Dosen</h3>

						

						<div class="right">
						<button type="button" class="btn" data-toggle="modal" data-target="#exampleModal">
						<i class="lnr lnr-pencil"></i></button>
					</div>


				</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<thead>
								<th>Nama</th>
                                <th>Telpon</th>
								<th>Alamat</th>
								@if(auth()->user()->role=='admin')
								<th>Aksi</th>
								@endif
						</thead>
                        <tbody>
							<tr>
													@foreach($data_guru as $guru)
									<td>{{$guru->nama}}</td>
									<td>{{$guru->telpon}}</td>
									<td>{{$guru->alamat}}</td>
									@if(auth()->user()->role=='admin')
									<td><a href="dosen/{{$guru->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
	<a href="#" class="btn btn-danger btn-sm delete " guru-id="{{$guru->id}}">Hapus</a></td>
							@endif
							</tr>
									@endforeach
						</tbody>
					</table>
                    
  </div>
			</div>
				</div>
			</div>
		</div>
	</div>
	
</div>


  <!--..........................................MODAL....................................................-->



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Dosen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">



				        <form action="dosen/create" method="POST">
				        	{{csrf_field()}}

				  <div class="form-group{{$errors->has('nama') ?'has-error' : '' }}">

				    <label for="exampleInputEmail1">Nama</label>
				    <input name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama" value="{{old('nama')}}">
				    @if($errors->has('nama'))
				    <span class="help-block">{{$errors->first('nama')}}</span>
				    @endif
				</div>

                
				  <div class="form-group{{$errors->has('telpon') ?'has-error' : '' }}">

            <label for="exampleInputEmail1">Telpon</label>
            <input name="telpon" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="telpon" value="{{old('telpon')}}">
            @if($errors->has('telpon'))
            <span class="help-block">{{$errors->first('telpon')}}</span>
            @endif
            </div>

		
				  <div class="form-group{{$errors->has('email') ?'has-error' : '' }}">
				    <label for="exampleInputEmail1">Email</label>
				    <input name="email" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email" value="{{old('email')}}">
				    @if($errors->has('email'))
				    <span class="help-block">{{$errors->first('email')}}</span>
				    @endif
			
				 </div>

				<div class="form-group">
		<label for="exampleFormControlTextarea1">Alamat</label>
		<textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="4">{{old('alamat')}}</textarea>

				   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <button type="submit" class="btn btn-primary">Tambah</button>

</div>                  
@stop


@section('footer')
<script>
	$('.delete').click(function () {
		var guru=$(this).attr('guru-id');

		swal({
  title: "Yakin ?",
  text: "Mau Menghapus guru Dengan ID"+guru+" ??",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
  	
    swal("Data Berhasil Di Hapus", "Hay {{auth()->user()->name}}", "success");
window.location="/dosen/"+guru+"/delete";

  } else {
    swal("Data Tidak Di Hapus", "Hay {{auth()->user()->name}}", "error");
  }
});

	});
</script>


@stop