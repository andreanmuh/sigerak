@extends('layout.master')	<!-- mengambil template dari file master yg ada di layout -->
		<!-- meletakkan semua cript di bawah section sampai endsection, ('konten')penamaan dibuat di file master-->

@section('konten')

<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Dosen</h3>
				</div>
				 <form action="/dosen/{{$guru->id}}/update" method="POST" enctype="multipart/form-data">
						        	{{csrf_field()}}

						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama</label>
						    <input name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama_depan" value="{{$guru->nama}}">

						    <br>

						    <label for="exampleInputEmail1">Telpon</label>
						    <input name="telpon" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama_belakang" value="{{$guru->telpon}}">

						    <br>

						    <label for="exampleFormControlTextarea1">Alamat</label>
						    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="4">{{$guru->alamat}}</textarea>   
						<br>

						  

						  </div>
						  <button type="submit" class="btn btn-warning">Update</button>
						</form>  


					
				</div>
			</div>

				</div>
			</div>
		</div>
	</div>
</div>

@stop




@section('konten1') 	
		<h1>Edit Data Dosen</h1>
		@if(session('sukses'))
		<div class="alert alert-success" role="alert">
		  {{session('sukses')}}
		</div>
		@endif

		<div v class="row">
			<div class="col-lg-12">
				------------------------------------------------------------------
					 <form action="/#/{{$guru->id}}/update" method="POST">
						        	{{csrf_field()}}

						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama Depan</label>
						    <input name="nama" type="text" class="form-control" id="exampleInputEmail1" 
                            aria-describedby="emailHelp" placeholder="nama_depan" value="{{$guru->nama_depan}}">
						    <br>
                            <div class="form-group">
						    <label for="exampleInputEmail1">Telpon</label>
						    <input name="telpon" type="text" class="form-control" id="exampleInputEmail1" 
                            aria-describedby="emailHelp" placeholder="nama_depan" value="{{$guru->nama_depan}}">
						    <br>

						    <label for="exampleFormControlTextarea1">Alamat</label>
						    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="4">{{$guru->alamat}}</textarea>   
						  </div>
						  <button type="submit" class="btn btn-warning">Update</button>
						</form>  
						</div>
						</div>  
@endsection